# The Ballad of Bard: Facing the Dragon of Symphony :musical_note: :dragon:

Enter the stage for a musical showdown with a dragon! Grab your lute, tune your lyre, and get ready to roll those d20s in the rhythm of destiny.

Prepare for a campaign where every roll is a note in the epic melody. Will you create a harmonious resolution, or will the dragon's dissonance reign supreme? Roll the dice, strum the chords, and let the musical adventure begin! :game_die: :musical_note:

## :microphone: Campaign

In the mystical land of Crescendia, a fearsome dragon named Dracophonius has hoarded an enchanted orchestra of legendary instruments. The bards of the realm have gone silent, and the once harmonious kingdom now echoes with dissonance. The player, a bold and tuneful bard, is chosen to embark on a quest to face the dragon and reclaim the lost instruments.

## :guitar: Character Creation

Class: Bard
Instrument Proficiency: Any musical instrument of your choice (be it a lute, lyre, or bagpipes).
Bardic Inspiration Twist: Instead of traditional inspiration, you can use your Bardic Inspiration to create magical harmonies that influence the outcome of your d20 rolls.

## :game_die: Roll for Rhythm

Roll your trusty 1d20, and let fate decide the fate of your melodies:

```py
import random

roll = random.randint(1, 20)

print(roll)
```

- **1-5: Out of Tune!** Your performance falls flat. The dragon laughs at your feeble attempt.
- **6-10: Mediocre Melody.** Your performance is average. The dragon seems unimpressed, but there's room for improvement.
- **11-15: Harmonious Hues.** Your music weaves a spell, capturing the dragon's attention. You're on the right track.
- **16-19: Virtuoso Vibes.** Your musical prowess dazzles the dragon, creating an opening for negotiation or a skillful escape.
- **20: Maestro of Miracles!** Your music transcends reality. The dragon is charmed, and the instruments respond to your call. The dragon may even become an unlikely ally.
